package com.itgacl.magic4j.libcommon.util.geohash;

import lombok.Data;

@Data
public class MBR {

    //最大纬度
    private double MaxLatitude;
    //最小纬度
    private double MinLatitude;
    //最大经度
    private double MaxLongitude;
    //最小经度
    private double MinLongitude;

    @Data
    public static class Point {
        private double latitude;
        private double longitude;
    }
}
