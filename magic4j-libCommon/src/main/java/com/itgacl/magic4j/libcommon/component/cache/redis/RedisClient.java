package com.itgacl.magic4j.libcommon.component.cache.redis;

import java.util.Set;

public interface RedisClient {

    String get(String key);

    byte[] get(byte[] key);

    /**
     * 以key-value 方式存储数据
     * @param key
     * @param value
     * @return
     */
    void set(String key, String value);

    /**
     * 以key-value 方式存储数据，可以设置过期时间
     * @param key
     * @param value
     * @param expire 过期时间，单位为秒
     * @return
     */
    void set(String key, String value, int expire);

    void set(byte[] key, byte[] value);

    void set(byte[] key, byte[] value, int expire);

    <T> T hget(String hkey, T key);

    /**
     * 存储结构化数据，一个hash存储一条数据，一个key存储一条数据中的一个属性，value则是属性对应的值。
     * @param hkey
     * @param key
     * @param value
     * @return
     */
    <T> void  hset(String hkey, T key, T value);

    /**
     * 对存储在指定key的数值执行原子的加1操作。
     * Redis的原子递增操作最常用的使用场景是计数器。
     * 使用思路是：每次有相关操作的时候，就向Redis服务器发送一个incr命令。
     * 例如这样一个场景：我们有一个web应用，我们想记录每个用户每天访问这个网站的次数。
     * web应用只需要通过拼接用户id和代表当前时间的字符串作为key，每次用户访问这个页面的时候对这个key执行一下incr命令。
     * @param key
     * @return
     */
    long incr(String key);

    /**
     * 对存储在指定key的数值执行原子的加integer操作。
     * @param key
     * @return
     */
    Long incr(String key, long integer);

    /**
     * 对存储在指定key的数值执行原子的加value操作。
     * @param key
     * @return
     */
    Double incr(String key, double value);

    /**
     * 对存储在指定key的数值执行原子的减1操作。
     * @param key
     * @return
     */
    Long decr(String key);

    /**
     * 对存储在指定key的数值执行原子的减integer操作。
     * @param key
     * @return
     */
    Long decr(String key, long integer);

    /**
     * 设置指定key的过期时间
     * @param key key
     * @param second 过期时间（单位：秒）
     * @return
     */
    void expire(String key, int second);

    /**
     * 返回给定 key 的剩余生存时间(TTL, time to live)
     * @param key
     * @return 剩余生存时间，单位秒
     */
    long ttl(String key);

    /**
     * 根据指定的key删除该key对应的value
     * @param key
     * @return
     */
    void del(String key);

    /**
     * 根据hkey和指定的key删除对应的value
     * @param key
     * @return
     */
    void hdel(String hkey, String key);

    /**
     * 根据指定的key删除该key对应的value
     * @param key
     * @return
     */
    void del(byte[] key);

    /**
     * 根据key存放对象
     * @param key
     * @param obj
     * @param <T>
     */
    <T> void setObject(String key, T obj);

    /**
     * 根据key获取对象
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T getObject(String key, Class<T> clazz);

    /**
     * 获取所有的key
     * @param pattern
     * @return
     */
    Set<String> keys(String pattern);
}
