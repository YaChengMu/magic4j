## 一、项目简介
一直都想按照自己的想法去开发一个基于SpringBoot的javaWeb快速开发平台(基础开发框架)，利用业余时间，从0到1，终于按照自己的想法去实现了一个基于SpringBoot的java快速开发平台，我将其命名为【magic4j】，寓意【神奇的、魔法般的Java开发平台】。

magic4j是一个【前后端分离】快速开发平台，支持【多租户】，基于 Spring Boot、mybatisPlus 和 Vue 开发，可以用于所有的Web应用程序，内置了许多通用性的功能模块，如用户管理、角色管理、资源管理等。magic4j旨在帮助中小型企业及个人实现项目的快速开发和交付。

## 二、主要特性

- 采用业界主流的【前后端分离】的开发模式，前端开源框架采用【vue-element-admin】 (基于 Vue、Element-UI)
- 后端采用【Springboot+Mybatis-Plus+jwt+redis】等主流的技术栈实现，并对其基础的【缓存组件】、【文件存储组件】做了高度的封装，提供了多种实现，做到开箱即用，多种实现方式灵活切换。
- 基于Mybatis-Plus实现的功能强大的【代码生成器】、根据自定义模板生成Entity、Mapper、Mppper.xml、DTO、Service、Controller代码，生成的文件直接到相应的业务模块，省去文件拷贝步骤，灵活可控。
- 采用全新的思路自主实现了基于【RBAC模型的权限管理】，实现了【菜单权限】、【功能权限】、【数据权限】的细致化访问权限控制。
- 支持【多租户】，租户数据隔离方案为：【共享数据库、Schema、数据表】，通过在表中增加【tenant_id】字段+【多租户SQL解析器】优雅地实现租户数据隔离。同时支持【一键开启或者关闭】多租户模式
- 使用【Mybatis拦截器】优雅的实现了数据权限，根据角色所拥有的数据权限范围在执行SQL查询过程中动态拼接过滤条件，实现数据过滤。同时支持【一键开启或者关闭】数据权限控制
- 支持【jwt和自定义Token】两种用户登录Token认证，系统默认使用的Jwt进行ToKen认证，可以通过修改application.yml配置文件切换Token的认证方式，根据情况自由选择。
- 自带了【缓存组件】，提供基于注解和API两种使用方式，基于【Redis和caffeine】两种实现方式，可以通过修改application.yml配置文件切换缓存的实现，Redis的方式又支持【单机和集群】两种模式。这样可以做到开发阶段使用caffeine、测试阶段使用Redis单机，生产使用Redis集群三种方式自由切换。
- 支持【本地】、【sftp】、【FastDFS】以及第三方【阿里云OSS】、【腾讯云COS】、【七牛云存储】多种文件存储服务，可动态任意切换。
- 注重【数据安全】传输，提供了【RSA和AES】两种加密方式，客户端通过调用RestFul API获取到RSA加密公钥和AES加密密钥，将客户端的敏感数据加密后传输。同时提供了通用的数据解密过滤器，在请求到达业务处理接口之前，将数据解密成明文，免去了接口手动处理解密步骤，专注业务实现逻辑。
- 注重【密码安全】传输，对于登录、修改密码、重置密码这些涉及到密码传输的操作都使用了RSA加密后再传输到服务端，避免了密码直接明文暴露传输。
- 支持【自定义密码验证规则】，可以灵活设置【密码长度】、至少包含的【大小写字母】、【数字】、【特殊字符】等组成规则，增加密码的复杂度
- 使用【ThreadLocal管理登录用户信息】实现随用随取，可以在任何方法中很方便的获取用户信息
- 注重日志信息收集，系统日志细分为【登录日志】和【操作日志】，详细记录的了用户的操作行为和登录行为，通过系统日志可以很清新地知道用户对系统进行的相关操作。
- 整合Quartz做【分布式定时任务】，并记录了任务【执行日志】，任务执行情况一目了然。
- 使用【拦截器实现防止重复提交】，保证一些关键性操作的幂等性。
- 完善的【用户】、【角色】、【菜单】、【组织机构】、【岗位】、【数据字典】、【系统配置】等基础功能。
- 接口访问统一采用标准的restful方式，集成swagger-ui在线接口文档，支持跨域请求调用，服务端采用主流的cros作为跨域解决方案。
- 支持【MySQL】和【Postgresql】两种主流数据库，其他主流的数据库Oracle和SQLServer理论上也都支持。
- 强大的底层封装，提供了自定义注解Auth、AutoIdempotent、Encrypt、Log、DataScope，优雅地实现了访问权限验证、方法幂等性校验，数据统一解密处理、AOP日志记录，数据权限控制。
- 自定义了【cloudStorage-spring-boot-starter】、【fastdfs-spring-boot-starter】、【jsch-spring-boot-starter】，方便以starter快速集成oss、cos、kodo、fastdfs、jsch。
- 大部分基础功能模块都支持Excel导入导出功能，导出到Excel支持自定义选择需要导出列，支持按选择的行进行导出。
- 基于Maven构建的多模块项目，支持分环境(test、prod)打包，预设了大量的环境变量，便于运维人员部署时动态替换相关配置(如DB连接地址、用户名、密码)

## 三、内置功能

1. 用户管理：管理系统用户，实现对用户增删改查、批量禁用和启用，重置密码，Excel导入导出
1. 部门管理：管理系统组织机构（公司、部门），树结构展现，支持数据权限。
1. 岗位管理：管理系统用户所属担任职务。
1. 菜单管理：管理系统菜单，树结构展现。
1. 角色管理：角色菜单权限、操作权限分配、设置角色按部门进行数据范围权限划分。
1. 租户管理：管理合作的租户信息
1. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
1. 安全设置：用于帐号锁定设置和配置密码验证规则。
1. 平台设置：用于设置系统的Logo和系统名称
1. 配置管理：对系统动态配置常用参数进行维护。
1. 操作日志：记录用户对系统进行的增删改行为，支持按操作用户、业务模块、操作结果、操作时间多维度组合的数据查询。
1. 登录日志：记录用户的登录操作，支持按登录用户、登录结果、登录时间多维度组合的数据查询。
1. 定时任务：在线添加、修改、删除、执行、暂停、恢复调度任务。
1. 调度日志：记录调度任务的执行情况。
1. 文件管理：普通文件上传、下载、管理上传文件。
1. 表单构建器：拖动表单元素生成相应的HTML代码。
1. 二维码管理：基于google的zxing实现的生成二维码功能。
1. 区域管理：对中国省市县行政区域基础数据进行管理
1. 用户访问地图：基于地图的方式统计来自各个省份城市的访问次数
1. 实用案例：百度地图集成案例

## 四、技术选型
### 4.1、开发环境

- JDK：JDK 1.8
- IDE： IntelliJ IDEA，安装lombok插件、MybatisX快速开发插件。MybatisX 是一款基于 IDEA 的快速开发插件，为效率而生。 安装方法：打开 IDEA，进入 File -> Settings -> Plugins -> Browse Repositories，输入 mybatisx 搜索并安装。
- 依赖管理：Maven3.x
- 数据库：MySQL5.7或者MySQL8
- 缓存：如果使用的是Redis缓存，则需要安装Redis
- 文件服务器：如果将文件上传到FastDFS，则需要搭建FastDFS文件服务器
- Nginx：负载均衡以及静态资源访问服务器，部署magic-ui前端项目需要使用到nginx

### 4.2、后端

- 基础框架：Spring Boot 2.2.6.RELEASE
- 持久化框架：Mybatis-Plus_3.4.0
- 数据库：MySQL
- 安全框架：权限拦截器+自定义注解实现类似Shiro的细粒度权限控制
- JwtToken生成与解析：jjwt_0.9.0
- 数据库连接池：Hikari，一款非常强大,高效,并且号称“史上最快连接池”。在SpringBoot2.0之后,采用的默认数据库连接池就是Hikari
- 缓存框架：redis+caffeine
- 文件存储：oss、cos、kodo、fastdfs、sftp
- 定时任务：quartz
- 日志框架：logback
- 工具包：hutool
- excel导入导出：easyexcel
- 二维码生成：zxing
- 自定义密码验证规则：passay
- 其他：fastjson，jsch, lombok等。

### 4.3、前端

- Vue2.6.10
- Vuex
- Vue Router
- Axios
- ElementUI2.13.0
- vue-element-admin(UI框架)
- jsencrypt(Rsa加密)
- crypto-js(Aes加密、sha256加密、md5加密、base64编码)

## 五、项目结构

### 5.1、magic4j服务端
![magic4j服务端](doc/images/magic4j服务端代码结构.png)

### 5.2、magic4j前端
![magic4j前端](doc/images/magic4j前端代码结构.png)

## 六、在线体验

### 6.1、演示
地址：[http://magic4j.itgacl.com](http://magic4j.itgacl.com/)
<br/>
演示账号：admin(系统管理员) 密码：123456

### 6.2、演示效果

#### 6.2.1、用户访问地理位置分布图
![用户访问地理位置分布图](doc/images/用户访问地理位置分布图.png)

#### 6.2.2、用户管理
![用户管理](doc/images/用户管理-1.png)
![用户管理](doc/images/用户管理-2.png)
![用户管理](doc/images/用户管理-3.png)
![用户管理](doc/images/用户管理-4.png)

#### 6.2.3、资源管理
![资源管理](doc/images/资源管理-1.png)

#### 6.2.4、角色管理
![角色管理](doc/images/角色管理-1.png)
![角色管理](doc/images/角色管理-2.png)
![角色管理](doc/images/角色管理-3.png)

#### 6.2.5、菜单管理
![菜单管理](doc/images/菜单管理-1.png)
![菜单管理](doc/images/菜单管理-2.png)

#### 6.2.6、系统配置
![系统配置](doc/images/系统配置.png)

#### 6.2.7、数据字典
![系统配置](doc/images/数据字典.png)

#### 6.2.8、安全设置
![安全设置](doc/images/安全设置.png)

#### 6.2.9、平台设置
![平台设置](doc/images/平台设置.png)

#### 6.2.10、区域管理
![区域管理](doc/images/区域管理.png)

#### 6.2.11、租户信息
![租户信息](doc/images/租户信息.png)

#### 6.2.12、租户用户
![租户用户](doc/images/租户用户.png)

#### 6.2.13、组织机构
![组织机构](doc/images/组织机构.png)

#### 6.2.14、岗位管理
![岗位管理](doc/images/岗位管理.png)

#### 6.2.15、文件管理
![文件管理](doc/images/文件管理-1.png)
![文件管理](doc/images/文件管理-2.png)

#### 6.2.16、登录日志
![登录日志](doc/images/登录日志.png)

#### 6.2.17、操作日志
![操作日志](doc/images/操作日志.png)

#### 6.2.18、任务管理
![任务管理](doc/images/任务管理-1.png)
![任务管理](doc/images/任务管理-2.png)
![任务管理](doc/images/任务管理-3.png)

#### 6.2.19、调度日志
![调度日志](doc/images/调度日志.png)

#### 6.2.20、接口文档
![接口文档](doc/images/接口文档.png)

#### 6.2.21、二维码管理
![二维码管理](doc/images/二维码管理-1.png)
![二维码管理](doc/images/二维码管理-2.png)


#### 6.2.22、地图集成案例
![地图集成案例](doc/images/地图集成案例.png)

## 七、成功案例

### 7.1、接龙小程序
![接龙小程序](doc/images/友友团接龙小程序-1.png)
![接龙小程序](doc/images/友友团接龙小程序-2.png)
![接龙小程序](doc/images/友友团接龙小程序-3.png)

### 7.2、共享雨伞小程序
![共享雨伞小程序](doc/images/共享雨伞小程序-1.png)
![共享雨伞小程序](doc/images/共享雨伞小程序-2.png)
![共享雨伞小程序](doc/images/共享雨伞小程序-3.png)

### 7.3、物联网应用平台
![物联网应用平台](doc/images/物联网应用平台-1.png)
![物联网应用平台](doc/images/物联网应用平台-2.png)

## 八、二次开发
1、获取服务端项目地址： [https://gitee.com/gacl/magic4j](https://gitee.com/gacl/magic4j)
<br />
2、获取前端UI项目地址：[https://gitee.com/gacl/magic4j-ui](https://gitee.com/gacl/magic4j-ui)
<br/>
3、magic4j技术文档：[https://www.yuque.com/guaocanglang/ckv3h8](https://www.yuque.com/guaocanglang/ckv3h8)


## 九、项目合作
### 9.1、关于我
本人有9年的软件开发经验，先后担任过技术主管，java高级工程师、技术负责人、java架构师，擅长架构设计，有多年的架构设计经验，现在是担任软件研发总监，擅长分布式JavaWeb应用，物联网应用，微信公众号、微信小程序的应用开发，擅长JavaEE开发中常用的SpringBoot、SpringCloud、Netty、JFinal、motan、shiro、MyBatis、MyBatisPlus、Spring-data-jpa、casandra、mongodb等框架，有基于motan和SpringCloud实现的微服务化的项目实战经验。有基于SpringBoot+Netty的物联网项目开发实战经验。
掌握Vue.js、webpack、node.js、element-ui、vue-element-admin等主流前端技术的使用

<br/>
技术博客：http://blog.itgacl.com

### 9.2、联系我
如有项目合作，或者使用magic4j的过程中遇到了问题，可以通过以下方式与我联系！

![](doc/images/qq.png)
![](doc/images/wx.png)

## 十、关注我
![](doc/images/公众号.png)