package com.itgacl.magic4j.modules.comm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 二维码信息表
 * </p>
 *
 * @author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @since 2021-03-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("comm_qrcode_info")
@ApiModel(value="CommQrcodeInfo对象", description="二维码信息表")
public class CommQrcodeInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "二维码文本内容")
    private String qrcodeText;

    @ApiModelProperty(value = "二维码图片链接")
    private String qrcodeUrl;

    @ApiModelProperty(value = "二维码顶部文字")
    private String topFont;

    @ApiModelProperty(value = "二维码中心文字")
    private String centerFont;

    @ApiModelProperty(value = "二维码底部文字")
    private String bottomFont;

    @ApiModelProperty(value = "租户ID")
    private Long tenantId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标识")
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleteFlag;


    public static final String ID = "id";

    public static final String QRCODE_TEXT = "qrcode_text";

    public static final String QRCODE_URL = "qrcode_url";

    public static final String TOP_FONT = "top_font";

    public static final String CENTER_FONT = "center_font";

    public static final String BOTTOM_FONT = "bottom_font";

    public static final String TENANT_ID = "tenant_id";

    public static final String CREATE_TIME = "create_time";

    public static final String UPDATE_TIME = "update_time";

    public static final String DELETE_FLAG = "delete_flag";

}
