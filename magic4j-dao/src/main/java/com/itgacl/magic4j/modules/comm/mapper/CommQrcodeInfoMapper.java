package com.itgacl.magic4j.modules.comm.mapper;

import com.itgacl.magic4j.modules.comm.entity.CommQrcodeInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 二维码信息表 Mapper 接口
 * </p>
 *
 * @author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @since 2021-03-05
 */
public interface CommQrcodeInfoMapper extends BaseMapper<CommQrcodeInfo> {

}
