# magic4j-application

## 服务说明

## 服务名称:
magic4j-application

## 当前版本
1.0.0

## 关联资源

## 关联组件
无
## 部署说明

### 1.配置参数

| 字段 | 说明 | 默认值 | 备注 |
| --- | --- | --- | --- |
|SERVER_PORT|服务启动端口|8888|服务启动端口，默认配置的使用端口是:8888|

### 2.打包应用(需要先安装maven)

#### 2.1.打测试包
> mvn clean package -Ptest

#### 2.2.打生产包
> mvn clean package -Pprod

### 3.启动应用

#### 3.1.进入magic4j-application-0.0.1-SNAPSHOT.jar所在目录
> eg: cd /data/htdocs

#### 3.2.启动 
> 1.默认启动，使用application.yml的默认配置项启动：
```
nohup java -jar magic4j-application-0.0.1-SNAPSHOT.jar > /dev/null 2>&1 & 
```
使用这种方式启动项目时不输出nohup.out文件

> 2.带命令行参数的启动方式：替换application.yml定义的配置参数(环境变量)，格式：--key=value
```
nohup java -jar magic4j-application-0.0.1-SNAPSHOT.jar --DATASOURCE_URL=47.11.12.23:3806/magic4j --DATASOURCE_USERNAME=magic4j --DATASOURCE_PASSWORD=abcd> /dev/null 2>&1 & 
```

# Centos8安装PostgreSQL
https://blog.csdn.net/weixin_43431593/article/details/106252231

# Linux 达梦数据库静默安装及使用详解
https://www.jianshu.com/p/7eb539f465a6


# drools 基于jar包实现动态规则
https://blog.csdn.net/u012373815/article/details/91549562

# 项目部署到linux系统后，生成的二维码无法显示中文的解决方案
在windows上找到一个字体文件，路径：C:\Windows\Fonts\simsun.ttc
然后到linux上，把那个simsun.ttc文件放到`JAVA_HOME/jre/lib/fonts`文件夹里去，如果没有fonts文件夹就自己创建就好了。
最后重启java项目。

# 解决上传文件出现问题 413 Request Entity Too Large（请求实体太大）
解决方案
找到自己主机的nginx.conf配置文件，打开
在http{}中加入 client_max_body_size 10m;
然后重新加载nginx配置文件
./nginx -s reload