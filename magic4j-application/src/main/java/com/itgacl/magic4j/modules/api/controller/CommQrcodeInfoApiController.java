package com.itgacl.magic4j.modules.api.controller;

import com.itgacl.magic4j.common.base.AppBaseController;

import com.itgacl.magic4j.modules.comm.service.CommQrcodeInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.*;


/**
 * @Classname CommQrcodeInfoController
 * @Description 二维码信息表 Controller
 * @Author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @Date 2021-03-05
 * @Version 1.0
 */
@Api(tags = "CommQrcodeInfo管理")
@RestController
@RequestMapping("/app/api/comm/qrcodeInfo")
public class CommQrcodeInfoApiController extends AppBaseController{

    @Autowired
    private CommQrcodeInfoService commQrcodeInfoService;
}