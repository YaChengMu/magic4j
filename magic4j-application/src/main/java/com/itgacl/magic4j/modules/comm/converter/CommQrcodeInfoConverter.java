package com.itgacl.magic4j.modules.comm.converter;

import com.itgacl.magic4j.common.converter.BaseConverter;
import com.itgacl.magic4j.modules.comm.entity.CommQrcodeInfo;
import com.itgacl.magic4j.modules.comm.vo.CommQrcodeInfoVo;
import com.itgacl.magic4j.modules.comm.dto.CommQrcodeInfoDTO;
import org.springframework.beans.BeanUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * @Classname CommQrcodeInfo转换器
 * @Description Entity、VO、DTO相互转换器
 * @Author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @Date 2021-03-05
 * @Version 1.0
 */
public class CommQrcodeInfoConverter extends BaseConverter<CommQrcodeInfo, CommQrcodeInfoVo, CommQrcodeInfoDTO> {

    public static CommQrcodeInfoConverter build() {
        return new CommQrcodeInfoConverter();
    }

    @Override
    public CommQrcodeInfoVo vo(CommQrcodeInfo entity) {
        CommQrcodeInfoVo commQrcodeInfoVo = new CommQrcodeInfoVo();
        BeanUtils.copyProperties(entity,commQrcodeInfoVo);
        return commQrcodeInfoVo;
    }

    @Override
    public CommQrcodeInfo entity(CommQrcodeInfoDTO entityDto) {
        CommQrcodeInfo commQrcodeInfo = new CommQrcodeInfo();
        BeanUtils.copyProperties(entityDto,commQrcodeInfo);
        return commQrcodeInfo;
    }

}
