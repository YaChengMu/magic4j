package com.itgacl.magic4j.modules.comm.query;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Classname CommQrcodeInfoQuery
 * @Description CommQrcodeInfo的Query对象
 * @Author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @Date 2021-03-05
 * @Version 1.0
 */
@ApiModel(value="CommQrcodeInfo查询对象")
@Data
public class CommQrcodeInfoQuery  implements Serializable {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "二维码文本内容")
    private String qrcodeText;

    @ApiModelProperty(value = "二维码顶部文字")
    private String topFont;

    @ApiModelProperty(value = "二维码中心文字")
    private String centerFont;

    @ApiModelProperty(value = "二维码底部文字")
    private String bottomFont;

    @ApiModelProperty(value = "租户ID")
    private Long tenantId;




}
