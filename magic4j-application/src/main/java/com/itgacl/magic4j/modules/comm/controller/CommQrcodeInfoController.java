package com.itgacl.magic4j.modules.comm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itgacl.magic4j.common.base.SuperController;
import com.itgacl.magic4j.common.bean.PageData;
import com.itgacl.magic4j.common.bean.PageParam;
import com.itgacl.magic4j.libcommon.annotation.Auth;
import com.itgacl.magic4j.libcommon.annotation.Log;
import com.itgacl.magic4j.libcommon.bean.R;
import com.itgacl.magic4j.libcommon.constant.Constants;
import com.itgacl.magic4j.libcommon.util.DateUtils;
import com.itgacl.magic4j.modules.comm.service.CommQrcodeInfoService;
import com.itgacl.magic4j.modules.comm.entity.CommQrcodeInfo;
import com.itgacl.magic4j.modules.comm.vo.CommQrcodeInfoVo;
import com.itgacl.magic4j.modules.comm.dto.CommQrcodeInfoDTO;
import com.itgacl.magic4j.modules.comm.query.CommQrcodeInfoQuery;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.*;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @Classname CommQrcodeInfoController
 * @Description 二维码信息表 Controller
 * @Author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @Date 2021-03-05
 * @Version 1.0
 */
@Api(tags = "CommQrcodeInfo管理")
//@Auth(name = "CommQrcodeInfo管理")//在类上标注了@Auth注解后这个类下的所有接口将被AuthInterceptor拦截进行访问权限控制
@RestController
@RequestMapping("/api/comm/qrcodeInfo")
public class CommQrcodeInfoController extends SuperController{

    @Autowired
    private CommQrcodeInfoService commQrcodeInfoService;

    /**
     * 创建
     * @param commQrcodeInfo
     * @return
     */
    @ApiOperation("新增")
    @Log(operation="创建",remark = "创建CommQrcodeInfo",moduleName = "CommQrcodeInfo管理") //在方法上标注@Log注解用于记录操作日志
    @PostMapping
    public R<Void> create(@RequestBody @Validated(Constants.Create.class) CommQrcodeInfoDTO commQrcodeInfo){
        commQrcodeInfoService.create(commQrcodeInfo);
        return R.ok();
    }

    /**
     * 更新
     * @param commQrcodeInfo
     * @return
     */
    @ApiOperation("修改")
    @Log(operation="修改",remark = "修改CommQrcodeInfo",moduleName = "CommQrcodeInfo管理")
    @PutMapping
    public R<Void> update(@RequestBody @Validated(Constants.Update.class) CommQrcodeInfoDTO commQrcodeInfo){
        commQrcodeInfoService.update(commQrcodeInfo);
        return R.ok();
    }

    /**
     * 根据ID查找
     * @param id
     * @return
     */
    @ApiOperation("根据ID查找")
    @Auth(isAuth = false)//不进行权限控制
    @GetMapping("/{id}")
    public R<CommQrcodeInfoVo> get(@PathVariable("id") Long id){
        CommQrcodeInfoVo commQrcodeInfoVo = commQrcodeInfoService.getCommQrcodeInfoById(id);
        return R.ok(commQrcodeInfoVo);
    }

    /**
     * 根据条件查询
     * @return
     */
    @ApiOperation("根据条件查询")
    @Auth(isAuth = false)//不进行权限控制
    @GetMapping
    public R<List<CommQrcodeInfoVo>> get(CommQrcodeInfoQuery query) {
        //构建查询条件
        QueryWrapper<CommQrcodeInfo> queryWrapper = buildQueryWrapper(query);
        List<CommQrcodeInfoVo> commQrcodeInfoListVo = commQrcodeInfoService.getList(queryWrapper);
        return R.ok(commQrcodeInfoListVo);
    }

    /**
    * 根据ID批量删除
    * @param ids
    * @return
    */
    @ApiOperation("根据ID批量删除")
    @Log(operation="删除",remark = "根据ID删除CommQrcodeInfo",moduleName = "CommQrcodeInfo管理")
    @DeleteMapping("/{ids}")
    public R<Void> delete(@PathVariable("ids") Long[] ids){
        if(ids.length==1){
            commQrcodeInfoService.deleteById(ids[0]);
        }else {
            List<Long> idList = Arrays.asList(ids);
            commQrcodeInfoService.deleteByIds(idList);
        }
        return R.ok();
    }

    @ApiOperation(value = "二维码图片打包下载", notes = "将二维码图片下载并打成zip包")
    @GetMapping("/zipDownload/{ids}")
    @ApiImplicitParam(name = "ids", value = "图片id", required = true)
    public void zipDownload(@PathVariable("ids") Long[] ids, HttpServletResponse response) {
        List<Long> idList = Arrays.asList(ids);
        commQrcodeInfoService.zipDownload(idList, response);
    }

    @PostMapping("/createQrcode/{ids}")
    @ApiOperation(value = "根据测站ID列表批量生成二维码图片", notes = "根据测站ID列表批量生成二维码图片")
    @ApiImplicitParam(name = "ids", value = "测站id", required = true)
    public R<Void> createQrcode(@PathVariable("ids") Long[] ids) {
        List<Long> idList = Arrays.asList(ids);
        commQrcodeInfoService.createQrcode(idList);
        return R.ok();
    }

    /**
      * 分页查询
      * @return
      */
    @ApiOperation("分页查询")
    @Auth(isAuth = false)//不进行权限控制
    @GetMapping(value = "/list")
    public R<PageData<CommQrcodeInfoVo>> pageList(CommQrcodeInfoQuery query, PageParam pageParam){
        //构建查询条件
        QueryWrapper<CommQrcodeInfo> queryWrapper = buildQueryWrapper(query);
        PageData<CommQrcodeInfoVo> pageData = commQrcodeInfoService.pageList(pageParam,queryWrapper);//mybatisPlus分页查询
        return R.ok(pageData);
    }

    /**
     * 构建查询条件QueryWrapper
     * @param query
     * @return
     */
    private QueryWrapper<CommQrcodeInfo> buildQueryWrapper(CommQrcodeInfoQuery query) {
        QueryWrapper<CommQrcodeInfo> queryWrapper = new QueryWrapper<>();
        //构建查询条件
        if(query.getId() != null){
            queryWrapper.eq(CommQrcodeInfo.ID,query.getId());
        }
        if(StrUtil.isNotBlank(query.getQrcodeText())){
            queryWrapper.like(CommQrcodeInfo.QRCODE_TEXT,query.getQrcodeText());
        }
        if(StrUtil.isNotBlank(query.getTopFont())){
            queryWrapper.like(CommQrcodeInfo.TOP_FONT,query.getTopFont());
        }
        if(StrUtil.isNotBlank(query.getCenterFont())){
            queryWrapper.like(CommQrcodeInfo.CENTER_FONT,query.getCenterFont());
        }
        if(StrUtil.isNotBlank(query.getBottomFont())){
            queryWrapper.like(CommQrcodeInfo.BOTTOM_FONT,query.getBottomFont());
        }
        if(query.getTenantId() != null){
            queryWrapper.eq(CommQrcodeInfo.TENANT_ID,query.getTenantId());
        }
        return queryWrapper;
    }
}