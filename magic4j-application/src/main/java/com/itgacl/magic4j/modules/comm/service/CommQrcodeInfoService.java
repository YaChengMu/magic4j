package com.itgacl.magic4j.modules.comm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itgacl.magic4j.modules.comm.dto.CommQrcodeInfoDTO;
import com.itgacl.magic4j.modules.comm.vo.CommQrcodeInfoVo;
import com.itgacl.magic4j.modules.comm.entity.CommQrcodeInfo;
import com.itgacl.magic4j.common.bean.PageData;
import com.itgacl.magic4j.common.bean.PageParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Classname CommQrcodeInfoService
 * @Description 二维码信息表 Service
 * @Author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @Date 2021-03-05
 * @Version 1.0
 */
public interface CommQrcodeInfoService extends IService<CommQrcodeInfo> {

    void create(CommQrcodeInfoDTO commQrcodeInfoDTO);

    void update(CommQrcodeInfoDTO commQrcodeInfoDTO);

 
    void deleteById(Long id);

    void deleteByIds(List<Long> idList);

    CommQrcodeInfoVo getCommQrcodeInfoById(Long id);

    void delete(QueryWrapper<CommQrcodeInfo> queryWrapper);

    void deleteAll();

    List<CommQrcodeInfoVo> getList(QueryWrapper<CommQrcodeInfo> queryWrapper);

    PageData<CommQrcodeInfoVo> pageList(Page<CommQrcodeInfo> page, QueryWrapper<CommQrcodeInfo> queryWrapper);

    PageData<CommQrcodeInfoVo> pageList(PageParam pageParam, QueryWrapper<CommQrcodeInfo> queryWrapper);

    /**
     * 二维码图片压缩打包下载
     *
     * @param ids      id集合
     * @param response
     */
    void zipDownload(List<Long> idList, HttpServletResponse response);

    /**
     * 生成二维码图片
     *
     * @param ids
     */
    void createQrcode(List<Long> idList);
}
