package com.itgacl.magic4j.modules.comm.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.itgacl.magic4j.common.base.BaseDTO;
import com.itgacl.magic4j.libcommon.constant.Constants;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

/**
 * @Classname CommQrcodeInfoDTO
 * @Description CommQrcodeInfo的数据传输对象（DTO)(Data Transfer Object)
 * @Author Created by xudp (alias:孤傲苍狼) 290603672@qq.com
 * @Date 2021-03-05
 * @Version 1.0
 */
@ApiModel(value="CommQrcodeInfo对象DTO", description="二维码信息表")
@Data
public class CommQrcodeInfoDTO extends BaseDTO implements Serializable {

    @NotBlank(message = "二维码文本内容",groups = Constants.Create.class)
    @ApiModelProperty(value = "二维码文本内容")
    private String qrcodeText;

    @ApiModelProperty(value = "二维码顶部文字")
    private String topFont;

    @ApiModelProperty(value = "二维码中心文字")
    private String centerFont;

    @ApiModelProperty(value = "二维码底部文字")
    private String bottomFont;
}
